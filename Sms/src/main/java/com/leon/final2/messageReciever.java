package com.leon.final2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by leontns on 12/3/13.
 * Broadcast reciever for Incoming Messages
 */

public class messageReciever extends BroadcastReceiver{


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle recieved = intent.getExtras();

        final Object[] pdusObj = (Object[]) recieved.get("pdus");

        for (int i = 0; i < pdusObj.length; i++) {

            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
            String phoneNumber = currentMessage.getDisplayOriginatingAddress();
            String message = currentMessage.getDisplayMessageBody();
            String date =String.valueOf(currentMessage.getTimestampMillis());

       Webservices.get(context).sendSingleMessage(message, phoneNumber, date);

    }
    }
}
