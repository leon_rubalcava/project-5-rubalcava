package com.leon.final2;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.leon.final2.Utilities.*;

/**
 * Created by leontns on 11/6/13.
 *
 * Web Services object sends all messages and to the webserver
 */

public class Webservices {

    private static Webservices sWebservices=null;
    private Context mAppContext;
    private String tok;
    private String phoneNumber;
    private String message;
    private JSONObject contactJSON;
    private JSONObject messageJSON;
    private JSONObject outboxJSON;
    private String date;
    private final String TAG ="GCM Webservices";

    GoogleCloudMessaging gcm;
    String regid;

    private Webservices( Context context){
        mAppContext = context;
    }

    public static Webservices get(Context c){

        if(sWebservices== null){
            sWebservices= new Webservices(c.getApplicationContext());

        }
        return sWebservices;
    }

    //May not need this


    /**
     * AsynTask for post
     *
     */
    private class Send extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params){
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://192.241.203.127/BlueText/userprocess.php");

        try{
            if ( gcm ==null){
                gcm = GoogleCloudMessaging.getInstance(mAppContext);

            }
            regid = gcm.register(SENDER_ID);
            String msg = "device registered "+ regid;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair(TOKEN, tok));
            nameValuePairs.add(new BasicNameValuePair(REGID, regid));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);
            String responseString = EntityUtils.toString(response.getEntity());
            String c;
            return true;
        }catch (ClientProtocolException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        return false;

    }
    }


    public boolean sendSingleMessage(String message, String phoneNumber, String date) {
        this.message= message;
        this.phoneNumber= phoneNumber;
        this.date= date;
        SendSingle a = new SendSingle();
        a.execute();
        return true;

    }

    public boolean setTok(String tok){
        this.tok = tok;
        Send a = new Send();
        a.execute();
        return true; //a.doInBackground();
    }

    public boolean sendCon (JSONObject contacts){
        this.contactJSON =contacts;
        SendContacts a = new SendContacts();
        a.execute();
        return true;
    }

    public boolean sendMessage (JSONObject message){
        this.messageJSON=message;
        SendMessages a = new SendMessages();
        a.execute();
        return true;
    }

    public boolean sendOutbox(JSONObject outbox){
        this.outboxJSON=outbox;
        SendOutbox a = new SendOutbox();
        a.execute();
        return true;
    }

    /**
     * AsynTask for post
     *
     */


private class SendContacts extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params){
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://192.241.203.127/BlueText/conversationProcess.php");

            try{
                contactJSON.put(TOKEN, tok);

                httpPost.setEntity(new StringEntity(contactJSON.toString(), "UTF-8"));
                //Headers
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept-Encoding", "application/json");
                httpPost.setHeader("Accept-Language", "en-US");

                HttpResponse response = httpClient.execute(httpPost);
                String responseString = EntityUtils.toString(response.getEntity());
                String c;

                return true;
            }catch (ClientProtocolException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;

        }
    }

    private class SendSingle extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params){
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://192.241.203.127/BlueText/singleMessage.php");

            try{
                JSONObject single = new JSONObject();
                single.put(TOKEN, tok);
                single.put(MESSAGE, message);
                single.put(NUMBER, phoneNumber);
                single.put(DATE, date);
                single.put(TYPE, TYPE_RECEIVED);
                httpPost.setEntity(new StringEntity(single.toString(), "UTF-8"));
                //Headers
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept-Encoding", "application/json");
                httpPost.setHeader("Accept-Language", "en-US");

                HttpResponse response = httpClient.execute(httpPost);
                String responseString = EntityUtils.toString(response.getEntity());

                return true;
            }catch (ClientProtocolException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;

        }
    }
    private class SendMessages extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params){
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://192.241.203.127/BlueText/message.php");

            try{
                messageJSON.put(TOKEN, tok);
                httpPost.setEntity(new StringEntity(messageJSON.toString(), "UTF-8"));
                //Headers
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept-Encoding", "application/json");
                httpPost.setHeader("Accept-Language", "en-US");
                //httpPost.setEntity(new StringEntity(list.toString()));
                //httpPost.setHeader("Accept", "application/json");
                //httpPost.setHeader("Content-type", "application/json");

                HttpResponse response = httpClient.execute(httpPost);
                String responseString = EntityUtils.toString(response.getEntity());
                String c;

                return true;
            }catch (ClientProtocolException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                   e.printStackTrace();
            }
            return false;

        }
    }
    private class SendOutbox extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params){
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://192.241.203.127/BlueText/outbox.php");

            try{

                outboxJSON.put(TOKEN, tok);
                httpPost.setEntity(new StringEntity(outboxJSON.toString(), "UTF-8"));
                //Headers
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept-Encoding", "application/json");
                httpPost.setHeader("Accept-Language", "en-US");

                HttpResponse response = httpClient.execute(httpPost);
                String responseString = EntityUtils.toString(response.getEntity());
                String c;

                return true;
            }catch (ClientProtocolException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;

        }
    }
}
