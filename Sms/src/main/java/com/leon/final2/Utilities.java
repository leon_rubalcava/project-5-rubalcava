package com.leon.final2;

/**
 * Created by leontns on 12/3/13.
 * Contains constants used through out the program
 */
public final class Utilities {

    static final String SERVER_URL= "http://192.241.203.127/BlueText/";
    static final String SENDER_ID= "498106320716";
    static final String DISPLAY_MESSAGE_ACTION = "com.leon.final2.DISPLAY_MESSAGE";
    static final String EXTRA_MESSAGE= "message";
    static final String MESSAGE="message";
    static final String DATE="date";
    static final String NUMBER="number";
    static final String NUMBER_CURSOR="address";
    static final String PERSON="person";
    static final String MESSAGE_BODY="body";
    static final String TYPE="type";
    static final String TYPE_SENT="sent";
    static final String TYPE_RECEIVED="received";
    static final String ID="id";
    static final String NAME="name";
    static final String CONTACTS="contacts";
    static final String INBOX="content://sms/inbox";
    static final String OUTBOX="content://sms/sent";
    static final String SEEN="seen";
    static final String READ="read";
    static final String TOKEN="token";
    static final String REGID="regid";
    static final int NUMBER_OF_MESSAGES_SENT=200;
}
