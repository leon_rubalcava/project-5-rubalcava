package com.leon.final2;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import static com.leon.final2.Utilities.*;

/**
 * Created by leontns on 11/29/13.
 */
public class JSONFactory {


    private static JSONFactory mJSONFactory;
    private Context mAppContext;

    private JSONFactory(Context context){
        mAppContext = context;
    }
    public static JSONFactory get(Context c){
        if(mJSONFactory== null){
            mJSONFactory= new JSONFactory(c.getApplicationContext());
        }
        return mJSONFactory;

    }





    public JSONObject contactInfo() throws JSONException {

        JSONObject contact = new JSONObject();
        JSONArray allContacts = new JSONArray();
        HashMap<String, Contact> createJson = contactHashmap();
        Iterator it = createJson.entrySet().iterator();

        while(it.hasNext()){
            JSONObject singleContact = new JSONObject();
            Map.Entry<String, Contact> convertMe = (Map.Entry) it.next();
            singleContact.put(NAME, convertMe.getValue().getName());
            singleContact.put(NUMBER, convertMe.getValue().getNumber());
            singleContact.put(ID, convertMe.getValue().getId());
            allContacts.put(singleContact);
    }
        contact.put(CONTACTS, allContacts);
        return contact;
    }


    private HashMap contactHashmap(){


        HashMap<String, Contact> contactMap= new HashMap<String, Contact>();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection =new String[] {ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor contactQuery = mAppContext.getContentResolver().query(uri,projection,null,null,null);

        while (contactQuery.moveToNext()){

            Contact add =new Contact(contactQuery.getString(contactQuery.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)),
                    contactQuery.getString(contactQuery.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)),
                    contactQuery.getString(contactQuery.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID)));

            contactMap.put(add.getId(), add);
        }
        return contactMap;

    }
        public JSONObject messageJSON(){
        JSONObject allMessage = new JSONObject();
        JSONArray arrayMessage = new JSONArray();
        Uri inbox =  Uri.parse(INBOX);
        Cursor cursor = mAppContext.getContentResolver().query(inbox, null, null, null, null);

        int count =0;
        try {
                while(cursor.moveToNext() && count< NUMBER_OF_MESSAGES_SENT )
                {
                        JSONObject singleMessage = new JSONObject();
                        String messageBody = cursor.getString(cursor.getColumnIndexOrThrow(MESSAGE_BODY));
                        String messageId= cursor.getString(cursor.getColumnIndexOrThrow(PERSON));
                        String messageSeen= cursor.getString(cursor.getColumnIndexOrThrow(SEEN));
                        String messageRead= cursor.getString(cursor.getColumnIndexOrThrow(READ));
                        String messageDate= cursor.getString(cursor.getColumnIndexOrThrow(DATE));
                        String messageAddress= cursor.getString(cursor.getColumnIndexOrThrow(NUMBER_CURSOR));
                    if(messageBody != null && messageBody.contains("\'")){
                        messageBody = messageBody.replace("\'", "\'\'");
                    }
                        singleMessage.put(MESSAGE, messageBody);
                        singleMessage.put(ID, messageId);
                        singleMessage.put(SEEN, messageSeen);
                        singleMessage.put(READ, messageRead);
                        singleMessage.put(DATE, messageDate);
                        singleMessage.put(NUMBER, messageAddress);
                        singleMessage.put(TYPE, TYPE_RECEIVED);
                        arrayMessage.put(singleMessage);
                        count++;
                }

            allMessage.put(MESSAGE, arrayMessage);
            return allMessage;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
     public JSONObject outboxJSON(){
        JSONObject allMessage = new JSONObject();
        JSONArray arrayMessage = new JSONArray();
        Uri outbox =  Uri.parse(OUTBOX);
        Cursor cursor = mAppContext.getContentResolver().query(outbox, null, null, null, null);

        int count =0;
        try {
                while(cursor.moveToNext() && count< NUMBER_OF_MESSAGES_SENT )
                {
                        JSONObject singleMessage = new JSONObject();
                        String messageBody = cursor.getString(cursor.getColumnIndexOrThrow(MESSAGE_BODY));
                        String messageId= cursor.getString(cursor.getColumnIndexOrThrow(PERSON));
                        String messageSeen= cursor.getString(cursor.getColumnIndexOrThrow(SEEN));
                        String messageRead= cursor.getString(cursor.getColumnIndexOrThrow(READ));
                        String messageDate= cursor.getString(cursor.getColumnIndexOrThrow(DATE));
                        String messageAddress= cursor.getString(cursor.getColumnIndexOrThrow(NUMBER_CURSOR));
                    if(messageBody != null && messageBody.contains("\'")){
                        messageBody = messageBody.replace("\'", "\'\'");
                    }
                        singleMessage.put(MESSAGE, messageBody);
                        singleMessage.put(ID, messageId);
                        singleMessage.put(SEEN, messageSeen);
                        singleMessage.put(READ, messageRead);
                        singleMessage.put(DATE, messageDate);
                        singleMessage.put(NUMBER, messageAddress);
                        singleMessage.put(TYPE, TYPE_SENT);
                        arrayMessage.put(singleMessage);
                        count++;
                }

            allMessage.put(MESSAGE, arrayMessage);
            return allMessage;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
