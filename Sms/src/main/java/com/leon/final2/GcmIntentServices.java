package com.leon.final2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by leontns on 12/4/13.
 * Process recieved Intent
 */
public class GcmIntentServices extends IntentService{
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public GcmIntentServices() {
        super("GcmIntentServices");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        String bo = extras.getString("message");
        String obo = extras.getString("phone");
       SmsManager sm =SmsManager.getDefault();
        sm.sendTextMessage(obo,null,bo,null,null);
        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }
}
