package com.leon.final2;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by leontns on 11/10/13.
 * Generates random access token for website login
 */
public class PasswordGenerator {

    Random rand = new Random();
    String done ="";
    MessageDigest md5 = null;

    /**
     *
     * @param hash byte[] must be at least 10 characters
     * @return
     */
    private String byteToHex(byte[] hash){
        Base64.encode(hash, 16);
        return new String( Base64.encode(hash, 16));
    }

    /**
     *
     * @return Password to user
     */
    public String getPassword(){
        try {
            md5= MessageDigest.getInstance("SHA-1");
            md5.reset();
            String a= Long.toHexString(rand.nextLong());
            md5.update(a.getBytes("UTF-8"));
            done = byteToHex(md5.digest());
        } catch (NoSuchAlgorithmException e) {
        }
        catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return done.substring(2, 6);
    }

}
