package com.leon.final2;

/**
 * Created by leontns on 11/26/13.
 * Represents a Contact
 */
public class Contact {

    private String name;
    private String number;
    private String id;

    public Contact(String name, String number, String id){
       this.name = name;
       this.number= number;
       this.id= id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }
}

