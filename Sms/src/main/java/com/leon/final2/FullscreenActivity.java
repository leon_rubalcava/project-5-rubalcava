package com.leon.final2;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONException;
/**
 *
 * @author Leon Rubalcava
 * Main Activity for BlueText
 */
public class FullscreenActivity extends Activity implements SensorEventListener, CompoundButton.OnCheckedChangeListener {


    PasswordGenerator pg = new PasswordGenerator();
    private SensorManager sensorManager;
    private Sensor mAcceleraomter;
    private float lastUpdate =0;
    private Switch mMotion;
    private Switch mBackup;
    private TextView mToken;
    private ToggleButton mBlueButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAcceleraomter = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, mAcceleraomter,sensorManager.SENSOR_DELAY_UI);
        mToken= (TextView)findViewById(R.id.movement_value);




        /////////////////////////////////////////////////////////////////////////////////////////////////////
        ///      ///////////     \\\\\\\\\\\        \\\\\\\\\\\     \\\\\\\\\\\
        ///          \\          \/       \/        /\              /\
        ///          \\          /\       /\        /\      \\\     /\      \\\
        ///          \\           \\\\\\\\\\        \\\\\\\\\\\\    \\\\\\\\\\\\
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        mBlueButton = (ToggleButton)findViewById(R.id.button);
        mBlueButton.setOnCheckedChangeListener(this);

        mMotion = (Switch) findViewById(R.id.motion);
        mMotion.setOnCheckedChangeListener(this);

        mBackup = (Switch) findViewById(R.id.backup);
        mBackup.setOnCheckedChangeListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, mAcceleraomter, SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        float sum =0;
        float a,b,c;
        a = event.values[0];
        b = event.values[1];
        c = event.values[2];
        sum = a+b+c;

        if( Math.abs((sum - lastUpdate)) >5  && mMotion.isChecked()){
            Toast.makeText(this, "App exiting because motion detection on",Toast.LENGTH_SHORT ).show();
            lastUpdate=sum;
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch(buttonView.getId()){
            case R.id.button:
                { if(isChecked == true){
                    // Create a Token and send it using webservices
                        String tok = pg.getPassword();
                        Webservices.get(this).setTok(tok);
                    //Get Contact Json Object from JSONFactory and send using webservices;
                    try{
                        Webservices.get(this).sendCon(JSONFactory.get(this).contactInfo());
                        Webservices.get(this).sendMessage(JSONFactory.get(this).messageJSON());
                        Webservices.get(this).sendOutbox(JSONFactory.get(this).outboxJSON());
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                        mToken.setText("Your token is " +tok);
                }
                    else{
                    finish();
                    Toast.makeText(this, "this will call stop method"  , Toast.LENGTH_SHORT).show();}
                }
                break;
            case R.id.motion:
                Toast.makeText(this, "this is motion" + (isChecked ? " on" : " off"), Toast.LENGTH_SHORT).show();
                break;
            case R.id.backup:
                Toast.makeText(this, "this is backup" + (isChecked ? " on" : " off"), Toast.LENGTH_SHORT).show();
                break;
        }
    }

}
