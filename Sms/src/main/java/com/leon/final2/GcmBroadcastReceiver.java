package com.leon.final2;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by leontns on 12/3/13.
 * Receives Broadcast from GCM and passes it on to the intent for proccessing
 */
    public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent){


        ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentServices.class.getName());

        startWakefulService(context,(intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
}
}
